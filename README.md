Referenced Content Moderation
=============================

About this module
-----------------
The Referenced Content Moderation module allows the user with the
"View the latest version" permission to:
	1. View the latest draft of the referenced content on the latest draft page
		of the parent content.
	2. View the latest draft of the referenced content on the view page
		of the parent content, if the parent content is in unpublished state.

Requirements
------------
The Referenced Content Moderation module requires the Content Moderation
core module.

Limitations
-----------
The Referenced Content Moderation module will disable the caches for
the node pages for the users with the "View the latest version" permission

Installation
------------
Install the Referenced Content Moderation module as you would normally
install a contributed Drupal module. Visit https://www.drupal.org/node/1897420
for further information.

Configuration
-------------
1. Navigate to Admin > Configuration > Workflow > Referenced Content Moderation
2. Select the referenced content types for which the latest draft has to be
	pulled on the latest draft page of the parent content.
3. Select the "Latest draft if the parent content is unpublished" checkbox and
	select the parent content type where the the latest draft of the referenced
	content has to be pulled if the parent content is unpublished.
