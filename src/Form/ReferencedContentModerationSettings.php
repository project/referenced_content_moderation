<?php

namespace Drupal\referenced_content_moderation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

/**
 * Referenced Content Moderation Settings.
 */
class ReferencedContentModerationSettings extends ConfigFormBase {

  /**
   * Provides Configuration Form name.
   */
  public function getFormId() {
    return 'referenced_content_moderation_settings_form';
  }

  /**
   * Provides Configuration Page name for Accessing the values.
   */
  protected function getEditableConfigNames() {
    return [
      "referenced_content_moderation.settings",
    ];
  }

  /**
   * Creates a Form for Configuring the Module.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config("referenced_content_moderation.settings");

    $form['ref_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Referenced Content Types'),
      '#default_value' => (!empty($config->get('ref_content_types'))) ? Json::decode($config->get('ref_content_types')) : [],
      '#required' => TRUE,
      '#description' => $this->t('Select all the referenced content types.'),
      '#options' => $this->getAllContentTypes(),
    ];
    $form['unpublished_node'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Latest draft if the parent content is unpublished'),
      '#default_value' => $config->get('unpublished_node'),
      '#description' => $this->t('If checked, The latest draft of the referenced content would be pulled if the parent content is unpublished.'),
    ];
    $form['parent_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Parent Content Types'),
      '#default_value' => (!empty($config->get('parent_content_types'))) ? Json::decode($config->get('parent_content_types')) : [],
      '#description' => $this->t('Select all the parent content types where the latest draft of the referenced content should be pulled.'),
      '#options' => $this->getAllContentTypes(),
      '#states' => [
        'visible' => [
          ":input[name='unpublished_node']" => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Validates the Configuration Form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('unpublished_node') && empty($this->getSelectedCheckboxValues($form_state->getValue('parent_content_types')))) {
      $form_state->setErrorByName('parent_content_types', $this->t('Please select the Parent Content Types'));
    }
  }

  /**
   * Submits the Configuration Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ref_content_types = Json::encode($this->getSelectedCheckboxValues($form_state->getValue('ref_content_types')));
    $parent_content_types = Json::encode($this->getSelectedCheckboxValues($form_state->getValue('parent_content_types')));

    // Retrieve the configuration.
    $this->configFactory->getEditable("referenced_content_moderation.settings")
      ->set('ref_content_types', $ref_content_types)
      ->set('unpublished_node', $form_state->getValue('unpublished_node'))
      ->set('parent_content_types', $parent_content_types)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to get all the content types.
   */
  private function getAllContentTypes() {
    $content_types = [];
    $all_content_types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    foreach ($all_content_types as $content_type_key => $content_type) {
      if (is_object($content_type)) {
        $content_types[$content_type->id()] = $content_type->label();
      }
    }
    return $content_types;
  }

  /**
   * Helper function to get all the selected checkbox value.
   */
  private function getSelectedCheckboxValues($checkbox_values) {
    foreach ($checkbox_values as $checkbox_value_key => $checkbox_value) {
      if ($checkbox_value == '0') {
        unset($checkbox_values[$checkbox_value_key]);
      }
    }
    return $checkbox_values;
  }

}
